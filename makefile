CC=gcc
CFLAGS_INC := -pthread -lpthread -lcrypto -lssl -lbsd 
CFLAGS := -g -O2 -Wall -pedantic $(CFLAGS_INC)

all: client server

client: client.c
	$(CC) -o tem client.c $(CFLAGS)

server: server.c
	$(CC) -o tem_server server.c $(CFLAGS)

install: tem tem_server
	mkdir -pv $(DESTDIR)$(PREFIX)/bin
	cp tem $(DESTDIR)$(PREFIX)/bin/tem
	cp tem_server $(DESTDIR)$(PREFIX)/bin/tem_server
	cp doc/tem.1 $(PREFIX)/man/man1/tem.1
	gzip $(PREFIX)/man/man1/tem.1
	cp doc/tem_server.1 $(PREFIX)/man/man1/tem_server.1
	gzip $(PREFIX)/man/man1/tem_server.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/tem*
	rm -f $(PREFIX)/man/man1/tem*

.PHONY: install uninstall
