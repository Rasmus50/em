#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h> 
#include <bsd/string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>

#include <time.h>

#define PORT 7272

#define ANSI_COLOR_RED	"\x1b[31m"	
#define ANSI_COLOR_GREEN "\x1b[32m"

typedef pthread_t OT_THREAD_HANDLE;

struct cli_info {
	int s;
	char *key;
};


char *xor_dencrypt(char *key, int keysize, char *message, int msize) {
	
	char *demessage = message;
	for(int i = 0; i < msize; i++) {
		demessage[i] = message[i] ^ key[i % keysize / sizeof(char)];
	}
	return demessage;
}

int save_to_file(char *buffer) {

	/* this function takes the buffer either from received messages or sent messages and saves them to a file "tem_log.tem" */

	char *str = buffer; 	
	FILE *fp;
			
	if((fp = fopen("tem_log.tem", "a")) == NULL) {
		perror("could not open file to write ");
		abort();
	}

	fprintf(fp, "%s", str);

	fclose(fp);
	free(buffer);
	
	return 0;
}

char *assemble_message(char *buffer) {
	
	time_t ltime;
	struct tm *info;
	char time_buffer[20];

	time(&ltime);
	info = localtime(&ltime);
	strftime(time_buffer, 20, "%x - %T:%M", info);
	
	strcat(buffer, "\t\t\t");
	strcat(buffer, time_buffer);
	strcat(buffer, "\n");

	return buffer;
}

void *receive(void *cli) {

	char *buffer;
	int len;
	struct cli_info ci = *(struct cli_info *)cli;
	
	buffer = (char *)calloc(256, 1);	

	while((len = recv(ci.s, buffer, 256, 0)) > 0) {
		xor_dencrypt(ci.key, strlen(ci.key), buffer, strlen(buffer));
		fputs(buffer, stdout);
		save_to_file(buffer);
		memset(buffer, '\0', sizeof(&buffer));
		printf("\n");
		}
	pthread_exit(NULL);
}

char *remove_newline(char *string) {
	char *newline;

	if((newline = strchr(string, '\n')) != NULL) {
		*newline = '\0';
	}
	else {
		puts("[-] error : string too large");
		abort();
	}

	return string;
}

int message_interface(int sock) {

	/* function to allow user to enter input and send it to server */

	char *buffer;
	int len;
	pthread_t receive_t;
	struct cli_info cli;
	cli.s = sock;
	cli.key = calloc(20, 1);
	
	
	puts("please enter a set of characters that have been agreed on by each party : ");
	while(fgets(cli.key, 20, stdin) == NULL) {
		remove_newline(cli.key);
	}
	len = strlen(cli.key);
	buffer = (char *)calloc(256, 1);
	if(pthread_create(&receive_t, NULL, receive, (void *)&cli) != 0) {
		perror("failed to create thread ");
		abort();
	}
	printf("Enter a message... : ");
	
	while(fgets(buffer, 202, stdin) != NULL) {	
		remove_newline(buffer);
		assemble_message(buffer);
		xor_dencrypt(cli.key, len, buffer, strlen(buffer));
		if((len = send(sock, buffer, strlen(buffer), 0)) < 0) {
			perror("could not send message");
			abort();
		}
		save_to_file(buffer);
		memset(buffer, '\0', strlen(buffer));
	}
	pthread_join(receive_t, NULL);	
	close(sock);
	return 0;	
}
	
int server_connect(char dest_ip[17]) { 

	/* function to connect the client to the specified server */
	
	int client_socket;
	
	//initialises a 'sockaddr_in' structure called 'server_addr'
	struct sockaddr_in server_addr;

	//creates the TCP client socket
	client_socket = socket(PF_INET, SOCK_STREAM, 0);	
	//overwrites any memory stored in 'server_addr' to null
	memset(&server_addr, '\0', sizeof(server_addr));
	// specifies family of addresses (IPv4)
	server_addr.sin_family=AF_INET;
	// specifies port the server will be using
	server_addr.sin_port=htons(PORT); 
	//specifies server IP address received from user
	server_addr.sin_addr.s_addr=inet_addr(dest_ip);

	if (connect(client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {	
		//prints an appropriate error message
		perror("[-] error connecting to server ");	
		abort();
	}

	printf("[+] successfully connected\n");


	return client_socket;

}

char *input_dest() {
	/* receives address to server */
	char *dest_ip;

	dest_ip = (char *)calloc(17,1);

	puts("please enter the destination IP address : ");
	while(fgets(dest_ip, 17, stdin) == NULL) {
		perror("could not read IP address ");
	}
	return dest_ip;
}

int main() {

	/* main function that takes address of a server to connect to */
	char *ip; 
	int sock;
	//clear terminal screen	
	if(system("clear") < 0) {
		perror("error clearing terminal ");
		abort();
	}
	ip = input_dest();
	sock = server_connect(ip);
	message_interface(sock);
	
	return 0;
}
