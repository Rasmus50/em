#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <errno.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>


#define PORT 7272
#define PENDMAX 3
#define MAXCON 30

struct account {
	char ip[INET_ADDRSTRLEN];
	int sockno;
};

struct account users[MAXCON];

int clients[MAXCON];
int n_of_cc = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void forward(char *buffer, int current) {

	//loop through array of conected sockets
	for(int i = 0; i < n_of_cc; i++) {
		//when the socket number isn't the same as the current connected client, forward message
		if(clients[i] != current) {
			if(send(users[i].sockno, buffer, strlen(buffer), 0) < 0) {
				perror("failed to send message");
				continue;
			}
		}
	}
}

void message_interface(struct account *acc) {

	/* thread to receive and forward messages */
	
	char *buffer = (char *)calloc(256, 1);
	struct account current = *((struct account *)acc);
	
	while((recv(current.sockno, buffer, 256, 0)) > 0) {
		printf("\n%s\n", buffer);
		forward(buffer, current.sockno);
		memset(buffer, '\0', sizeof(*buffer));	
	}
	printf("user on %s has disconnected\n", current.ip);
	n_of_cc--;
	pthread_exit(NULL);

}
	
void *handle(void *sock) {

	struct account user = *((struct account *)sock);

	users[n_of_cc] = user;
	n_of_cc++;
	message_interface(&user);

	return 0;
}
int main() { 

	/* main function that accepts incoming connections from clients and creates a new thread to keep track of each one */

	 int sockfd, new_socket; //activity;
	 struct sockaddr_in server_addr, new_addr; 
	 pthread_t handle_t;
	 struct account user;
 	 socklen_t addr_size;  

	 system("clear");

	 sockfd = socket(PF_INET, SOCK_STREAM, 0);
	 memset(&server_addr, '\0', sizeof(server_addr));
	
	 if (sockfd == -1) {
		 perror("[-] error opening socket ");
	 }
	 else {
		 printf("[+] socket successfully opened\n");
	 }
	
	 /* set address information */
	 server_addr.sin_family=AF_INET;
	 server_addr.sin_port=htons(PORT);
	 server_addr.sin_addr.s_addr= INADDR_ANY;
	
	 if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {
		 perror("[-] error binding socket to address");
	 }
	 else { 
		 puts("[+] socket successfully bound to port");
	 }
	
	 if (listen(sockfd, PENDMAX) < 0) {
		 perror("[-] error listening ");
	 }
	 else {
 		 printf("[+] listening on port %i...\n", PORT);
	 }
			
	 /* monitoring multiple clients */
	
	 while(1) {
		 new_socket = accept(sockfd, (struct sockaddr*)&new_addr, &addr_size);
		 if(new_socket < 0) {
			 exit(1);
		 }
		 printf("user has connected on %s\n", inet_ntoa(new_addr.sin_addr));
		 user.sockno = new_socket;
		 strcpy(user.ip, inet_ntoa(new_addr.sin_addr));
		 pthread_create(&handle_t, NULL, handle, &user);
	 }
	 return 0;
}

